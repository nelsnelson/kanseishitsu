# kanseishitsu

A user who wishes to define their own cron jobs and file monitors in macOS faces some obstacles.

As of 2024, the current state of the art relies on XML plist files to define jobs in the `Library/LaunchAgents` directory in a user's home directory.

XML is not a very simple format for a human being to read or write.

If you already have the skills to read and write macOS LaunchAgent XML documents with ease, then the authors commend you for your determination and studiousness.

Otherwise, for all of you lazy and ignorant people like the authors, this gem is for you!

The `kanseishitsu` gem provides some little ruby scripts which will support simpler management of such launch agents.


## Usage

Here is some usage and examples for each script.


### cron.rb

The usage for `cron.rb`.

The `cron.rb` executable uses something similar to a  `crontab` entry in GNU/Linux.

```sh
$ cron.rb --help
Usage: cron.rb <crontab>|<options>

Options:
        --show-all                   Show all cron jobs
    -l, --list                       List launch agent labels
    -r, --remove=<label>             Remove a cron job by label
```


#### Example

For example:

```sh
cron.rb "0 1 * * * archive.rb ~/Downloads/archive"
```


### watch.rb

The usage for `watch.rb`.

```sh
$ watch.rb --help
Usage: watch.rb <watch_path> <handler_program_path>

Options:
        --show-all                   Show watchers
    -l, --list                       List launch agent labels
    -r, --remove=<label>             Remove a watcher by label
```


#### Example

For example:

```sh
watch.rb ~/Desktop ~/.local/usr/bin/desktop.rb
```


## Docker

Build the docker image to support CI/CD containers.

```sh
docker buildx build --tag="$(basename $(pwd))" .
docker run --interactive --tty --rm --name "$(basename $(pwd))" "$(basename $(pwd))"
```


## Name meaning that nobody should really care about

The Japanese word "管制室" (Kanseishitsu) translates to "control room" in English. It refers to a room from which operations are directed or monitored, typically involving equipment and personnel to oversee and manage various functions such as in factories, airports, or television studios.

Pronounced: Kahn-say-she-tsu

Here's how to say each part:

- Kahn: like the beginning of "con" in "convenient," but with a more open 'a' sound.
- Say: just like the English word "say."
- She: as in "she."
- Tsu: this part can be a bit tricky because it's not a sound typically found in English. It's similar to saying "sue," but start it with a soft 't' sound placed right before the 's'.


## Project file tree

Here is a bird's-eye view of the project layout.

```sh
date && tree -A -I "Gemfile.lock|*.gem|tmp|vendor"
Wed May 15 23:07:43 CDT 2024
.
├── Dockerfile
├── Gemfile
├── LICENSE.md
├── README.md
├── Rakefile
├── bin
│   ├── cron.rb
│   └── watch.rb
├── kanseishitsu.gemspec
├── kanseishitsu.png
├── lib
│   ├── kanseishitsu
│   │   ├── cron
│   │   │   ├── argument_parser.rb
│   │   │   └── launch_agent_manager_rexml.rb
│   │   ├── cron.rb
│   │   ├── launch_agent_constants.rb
│   │   ├── launch_agent_manager.rb
│   │   ├── logging.rb
│   │   ├── plist_parser.rb
│   │   ├── version.rb
│   │   ├── watch
│   │   │   └── argument_parser.rb
│   │   ├── watch.rb
│   │   ├── which.rb
│   │   ├── xml_helper.rb
│   │   └── xml_helper_rexml.rb
│   └── kanseishitsu.rb
├── scripts
│   └── version_bump.sh
└── spec
    ├── spec_helper.rb
    ├── test_spec.rb
    └── verify
        ├── launchctl.rb
        └── verify_spec.rb

8 directories, 28 files
```