#! /usr/bin/env ruby

# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# Copyright Nels Nelson 2024 but freely usable (see license)

# Usage: watch.rb <path> [handler_program_path]
#
# Options:
#         --show-all                   Show watchers
#     -l, --list                       List launch agent labels
#     -r, --remove=<label>             Remove a watcher by label
#
# Examples:
#
# watch.rb ~/Desktop ~/.local/usr/bin/desktop.rb
# watch.rb --show-all
# watch.rb --list
# watch.rb --remove=com.local.desktop

require_relative '../lib/kanseishitsu'

Object.new.extend(Watcher).main
