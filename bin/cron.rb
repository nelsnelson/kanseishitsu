#! /usr/bin/env ruby

# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# Copyright Nels Nelson 2024 but freely usable (see license)

# Usage: cron.rb <crontab>|<options>
#
# Options:
#         --show-all                   Show cron jobs
#     -l, --list                       List cron jobs labels
#     -r, --remove=<label>             Remove a cron job by label
#
# Examples:
#
# cron.rb "0 1 * * * archive.rb ${HOME}/Downloads/archive"
# cron.rb --show-all
# cron.rb --list
# cron.rb --remove=com.local.archive

require_relative '../lib/kanseishitsu'

Object.new.extend(Cron).main
