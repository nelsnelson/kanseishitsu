# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# Copyright Nels Nelson 2024 but freely usable (see license)

require 'optparse'

require_relative '../which'
require_relative '../version'

# Define module Watcher
module Watcher
  # Define the ArgumentsParser class
  class ArgumentsParser
    UNDERSCORE_PATTERN = %r{_}
    HYPHEN_STRING = '-'.freeze
    FLAGS = %i[banner show_all list remove verbose].freeze
    POSITIONAL = %i[watch_path executable_path_with_args].freeze
    attr_reader :parser, :options

    def initialize(parser = OptionParser.new)
      @parser = parser
      @options = {}
      FLAGS.each { |method_name| self.method(method_name).call }
    end

    def banner
      @parser.banner = "Usage: #{File.basename($PROGRAM_NAME)} " \
        '<watch_path> <executable_path_with_args>'
      @parser.separator ''
      @parser.separator 'Options:'
    end

    def show_all
      @parser.on('--show-all', 'Show watchers') do
        @options[:show_all] = true
      end
    end

    def list
      @parser.on('-l', '--list', 'List watcher labels') do
        @options[:list] = true
      end
    end

    def remove
      @parser.on('-r', '--remove=<label>', 'Remove a watcher by label') do |label|
        @options[:remove] = label
      end
    end

    def verbose
      @options[:log_level] ||= Logger::INFO
      @parser.on_tail('-v', '--verbose', 'Increase verbosity') do
        @options[:log_level] -= 1
      end
    end

    def version
      @parser.on_tail('--version', 'Show version') do
        puts "#{File.basename($PROGRAM_NAME)} version #{Kanseishitsu::VERSION}"
        exit
      end
    end

    def watch_path(args)
      return if (v = args.shift).nil?

      watch_path = File.expand_path(v)
      unless File.exist?(watch_path) &&
          File.directory?(watch_path)
        message = "Directory not found: #{watch_path}"
        raise OptionParser::InvalidArgument, message
      end
      @options[:watch_path] = [watch_path]
    end

    def executable_path_with_args(args)
      executable, *args = args
      return if executable.nil?

      executable_path = Object.new.extend(Which).which(executable)
      if executable_path.nil?
        message = "Executable not found: #{executable_path}"
        raise OptionParser::InvalidArgument, message
      end
      @options[:executable_path_with_args] = [executable_path] + args
    end

    def demand(arg, positional: false)
      return @options[arg] unless @options[arg].nil?

      required_arg = if positional then "<#{arg}>"
      else "--#{arg.to_s.gsub(UNDERSCORE_PATTERN, HYPHEN_STRING)}"
      end
      raise OptionParser::MissingArgument, "Required argument: #{required_arg}"
    end

    def positional!(args)
      POSITIONAL.each do |opt|
        self.method(opt).call(args)
        self.demand(opt, positional: true)
      end
    end

    def options?
      ArgumentsParser::FLAGS.any? { |flag| @options.include?(flag) }
    end

    def usage!
      puts @parser
      exit
    end

    # rubocop: disable Metrics/MethodLength
    def self.parse(args = ARGV, _file_path = ARGF, arguments_parser = ArgumentsParser.new)
      arguments_parser.parser.parse!(args)
      if !arguments_parser.options? && ARGV.length != 2
        message = 'A directory and executable handler is required'
        raise OptionParser::MissingArgument, message
      elsif !args.empty?
        arguments_parser.positional!(args)
      end
      arguments_parser.options
    rescue OptionParser::AmbiguousOption => e
      abort e.message
    rescue OptionParser::ParseError => e
      puts e.message
      arguments_parser.usage!
    end
    # rubocop: enable Metrics/MethodLength
  end
  # class ArgumentsParser
end
# module Watcher
