# encoding: utf-8
# frozen_string_literal: false

# vi: set ft=ruby :
# -*- mode: ruby -*-

# Copyright Nels Nelson 2024 but freely usable (see license)

# Define module XMLHelperInstanceMethods
module XMLHelperInstanceMethods
  CDATA_PATTERN = /[<>&'"\n]+/
  KEYS_REQUIRING_CDATA = ['PATH'].freeze

  def create_xml_tag(doc, parent, key, value)
    element = case value
    when Hash then create_dict(doc, value)
    when Array then create_array(doc, value)
    else create_simple_element(doc, key, value)
    end
    parent << doc.create_element('key', key.to_s) unless element.nil?
    parent << element unless element.nil?
  end

  def create_dict(doc, value)
    dict = doc.create_element('dict')
    value.each do |k, v|
      create_xml_tag(doc, dict, k, v)
    end
    dict
  end

  def create_array(doc, value)
    array = doc.create_element('array')
    value.each do |v|
      string = doc.create_element('string', v.to_s)
      array << string
    end
    array
  end

  def create_simple_element(doc, key, value)
    # Decide whether to wrap value in CDATA tag specifier
    if value.to_s.match?(CDATA_PATTERN) || KEYS_REQUIRING_CDATA.include?(key.to_s)
      cdata = doc.create_cdata(value.to_s)
      string_value = doc.create_element('string')
      string_value << cdata
      string_value
    else
      doc.create_element(value.class.to_s.downcase, value.to_s)
    end
  end
end
# module XMLHelperInstanceMethods
