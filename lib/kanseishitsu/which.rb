# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# Copyright Nels Nelson 2024 but freely usable (see license)

# Define module Which
module Which
  # Find the full path to a given executable in the PATH
  # system environment variable
  def find_executable_in_path(cmd)
    return if cmd.nil?

    directory = ENV['PATH'].split(File::PATH_SEPARATOR).find do |dir|
      File.executable?(File.join(dir, cmd))
    end
    directory.nil? ? nil : File.join(directory, cmd)
  end

  def executable?(exe)
    !exe.empty? && File.executable?(exe) && !File.directory?(exe)
  end

  def explicit_which(cmd)
    exe = `which #{cmd}`.chomp
    executable?(exe) ? exe : nil
  rescue Errno::ENOENT => _e
    nil
  end

  def portable_which(cmd)
    explicit_which(cmd) || find_executable_in_path(cmd)
  end
  alias which portable_which
end
# module Which
