#! /usr/bin/env ruby
# encoding: utf-8
# frozen_string_literal: false

# vi: set ft=ruby :
# -*- mode: ruby -*-

# Copyright Nels Nelson 2024 but freely usable (see license)

# Usage: cron.rb <crontab>|<options>
#
# Options:
#         --show-all                   Show cron jobs
#     -l, --list                       List cron jobs labels
#     -r, --remove=<label>             Remove a cron job by label
#
# Examples:
#
# cron.rb "0 1 * * * archive.rb ${HOME}/Downloads/archive"
# cron.rb --show-all
# cron.rb --list
# cron.rb --remove=com.local.archive

require_relative 'launch_agent_manager'
require_relative 'cron/argument_parser'

# Define module Cron
module Cron
  LaunchAgentManager = ::LaunchAgentManager

  # rubocop: disable Metrics/MethodLength
  def main(args = Cron::ArgumentsParser.parse)
    log.log_level = args[:log_level]
    manager = Cron::LaunchAgentManager.new(args)
    if args[:show_all]
      manager.show_all_launch_agents
    elsif args[:list]
      manager.list_launch_agent_labels
    elsif args[:remove]
      manager.remove_launch_agent(args[:remove])
    else
      manager.create_launch_agent(
        args[:executable_path_with_args],
        nil,
        args[:cron_schedule]
      )
    end
  end
  # rubocop: enable Metrics/MethodLength
end
# module Cron

Object.new.extend(Cron).main if $PROGRAM_NAME == __FILE__
