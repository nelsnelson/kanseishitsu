# encoding: utf-8
# frozen_string_literal: false

# vi: set ft=ruby :
# -*- mode: ruby -*-

# Copyright Nels Nelson 2024 but freely usable (see license)

module Kanseishitsu
  VERSION = '0.1.1'.freeze
end
