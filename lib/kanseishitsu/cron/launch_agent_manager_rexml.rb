# encoding: utf-8
# frozen_string_literal: false

# vi: set ft=ruby :
# -*- mode: ruby -*-

# Copyright Nels Nelson 2024 but freely usable (see license)

require 'rexml/formatters/pretty'

require 'fileutils'

require_relative '../launch_agent_plist'

# Define module LaunchAgentManagerConstants
module LaunchAgentManagerConstants
  VERSION = '0.1.0'.freeze unless defined?(VERSION)
  LABEL_NAMESPACE = %w[com local].freeze
  INTERVALS = %w[Minute Hour Day Month Weekday].freeze
  SCHEDULE_PARTS_COUNT = INTERVALS.length
  LAUNCHCTL_TEMPLATE = 'launchctl bootstrap gui/%<uid>s %<plist>s'.freeze
  SUCCESS_MESSAGE = 'Created and enabled launchd job: %<label>s'.freeze
  LAUNCH_AGENTS_DIR_PATH = File.expand_path(File.join(Dir.home, 'Library', 'LaunchAgents'))
  BOOTOUT_TEMPLATE = 'launchctl bootout gui/%<uid>s/%<label>s'.freeze
  REMOVE_TEMPLATE = 'launchctl remove gui/%<uid>s/%<label>s'.freeze
end

# Define module LaunchAgentManagerInstanceMethods
module LaunchAgentManagerInstanceMethods
  include LaunchAgentManagerConstants

  def find_executable_in_path(cmd)
    directory = ENV['PATH'].split(File::PATH_SEPARATOR).find do |dir|
      File.executable?(File.join(dir, cmd))
    end
    directory.nil? ? nil : File.join(directory, cmd)
  end

  def executable?(exe)
    !exe.empty? && File.executable?(exe) && !File.directory?(exe)
  end

  def explicit_which(cmd)
    exe = `which #{cmd}`.chomp
    executable?(exe) ? exe : nil
  rescue Errno::ENOENT => _e
    nil
  end

  def portable_which(cmd)
    explicit_which(cmd) || find_executable_in_path(cmd)
  end

  # Extract program path and arguments, verifying executable
  # rubocop: disable Metrics/MethodLength
  def extract_program_path(executable_with_args)
    *args = executable_with_args.split
    exe = args.shift
    exe_path = File.exist?(exe) ? exe : portable_which(exe)
    if exe_path.nil? || !File.exist?(exe_path)
      abort "Cannot find executable in path: #{exe}"
    end
    if exe_path.nil? || !File.executable?(exe_path) || File.directory?(exe_path)
      abort "Given file is not executable: #{exe}"
    end
    args.unshift(exe_path)
    args
  end
  # rubocop: enable Metrics/MethodLength

  # Create the job name based on the given executable file path
  def derive_job_label_from_executable(exe_path)
    label = LABEL_NAMESPACE.dup
    label << File.basename(exe_path, '.*')
    label.join('.')
  end

  # Define cron settings for plist
  def derive_calendar_interval(cron_schedule)
    cron_fields = cron_schedule.split
    calendar_interval = {}
    INTERVALS.each do |key|
      value = cron_fields.shift
      raise "Invalid cron string: #{cron_schedule}" if value.nil? || value.empty?

      calendar_interval[key] = value
    end
    calendar_interval
  end

  # Save plist file to LaunchAgents directory; do not overwrite an
  # already existing file with the same name at the generated path
  # rubocop: disable Metrics/MethodLength
  def save_plist(label, doc)
    FileUtils.mkdir_p(LAUNCH_AGENTS_DIR_PATH)
    plist_path = File.join(LAUNCH_AGENTS_DIR_PATH, "#{label}.plist")
    puts "plist_path: #{plist_path}"
    return plist_path if File.exist?(plist_path)

    document = ''
    formatter = REXML::Formatters::Pretty.new(4)
    formatter.write(doc, document)
    puts '[DEBUG] Contents of plist xml document:'
    puts document
    File.write(plist_path, document)
    plist_path
  end
  # rubocop: enable Metrics/MethodLength

  def execute(command)
    puts "[DEBUG] Executing command: #{command}"
    system(command)
  end

  # Load the launchd job
  def load_launchd_job(plist_path)
    execute(format(LAUNCHCTL_TEMPLATE, uid: Process.uid, plist: plist_path))
  end

  # Show the launchd agents as crontabs
  def show_all_cron_jobs
    Dir.glob(File.join(LAUNCH_AGENTS_DIR_PATH, '*.plist')).map do |plist_path|
      job = parse_plist(plist_path)
      puts "#{job[:schedule]} #{job[:command]}"
    end
  end

  # Return a list of all user launchd agent labels
  def launch_agent_labels
    plist_file_paths = Dir.glob(File.join(LAUNCH_AGENTS_DIR_PATH, '*.plist'))
    plist_file_paths.each_with_object([]) do |file_path, labels|
      label = LaunchAgentPlist.parse(file_path)[:label]
      labels << label unless label.nil?
    end
  end

  # List labels of launchd agents
  def list_launch_agent_labels
    launch_agent_labels.each do |label|
      puts label
    end
  end

  # Remove plist Launch Agents by label
  def remove_cron_job(label)
    # warn "[DEBUG] Removing launch agent: #{label}"
    plist_path = File.expand_path(File.join(LAUNCH_AGENTS_DIR_PATH, "#{label}.plist"))
    # warn "[DEBUG] Removing launch agent plist definition file: #{plist_path}"
    if File.exist?(plist_path)
      execute(format(BOOTOUT_TEMPLATE, uid: Process.uid, label: label))
      execute(format(REMOVE_TEMPLATE, uid: Process.uid, label: label))
      FileUtils.rm(plist_path)
      puts "Removed launch agent: #{label}"
    else
      warn "Not found; crontab or launch agent: #{label}"
    end
  end

  # Create the launchd job
  def create_launchd_job(options)
    args = extract_program_path(options[:executable_path_with_args])
    label = derive_job_label_from_executable(args.first)
    calendar_interval = derive_calendar_interval(options[:cron_schedule])
    plist_doc = LaunchAgentPlist.new(label, args, calendar_interval).to_doc
    plist_path = save_plist(label, plist_doc)
    puts "Executing: cat #{plist_path}"
    puts `cat #{plist_path}`.strip
    load_launchd_job(plist_path)
    puts format(SUCCESS_MESSAGE, label: label)
  end
end
# module LaunchAgentManagerInstanceMethods

# Define the LaunchAgentManager class
class LaunchAgentManager
  include LaunchAgentManagerInstanceMethods

  attr_reader :options

  def initialize(options)
    @options = options
  end
end
