# encoding: utf-8
# frozen_string_literal: false

# vi: set ft=ruby :
# -*- mode: ruby -*-

# Copyright Nels Nelson 2024 but freely usable (see license)

require 'logger'
require 'optparse'

require_relative '../version'

# Define module Cron
module Cron
  # Define the ArgumentsParser class
  class ArgumentsParser
    FLAGS = %i[banner show_all list remove verbose version].freeze
    POSITIONAL = %i[crontab].freeze
    attr_reader :parser, :options

    def initialize(option_parser = OptionParser.new)
      @parser = option_parser
      @options = {}
      FLAGS.each { |method_name| self.method(method_name).call }
    end

    def banner
      @parser.banner = "Usage: #{File.basename($PROGRAM_NAME)} <crontab>|<options>"
      @parser.separator ''
      @parser.separator 'Options:'
    end

    def show_all
      @parser.on('--show-all', 'Show cron jobs') do
        @options[:show_all] = true
      end
    end

    def list
      @parser.on('-l', '--list', 'List cron jobs labels') do
        @options[:list] = true
      end
    end

    def remove
      @parser.on('-r', '--remove=<label>', 'Remove a cron job by label') do |label|
        @options[:remove] = label
      end
    end

    def verbose
      @options[:log_level] ||= Logger::INFO
      @parser.on_tail('-v', '--verbose', 'Increase verbosity') do
        @options[:log_level] -= 1
      end
    end

    def version
      @parser.on_tail('--version', 'Show version') do
        puts "#{File.basename($PROGRAM_NAME)} version #{Kanseishitsu::VERSION}"
        exit
      end
    end

    def crontab(args)
      crontab = args.shift.gsub(/\A['"]|['"]\z/, '').split
      @options[:crontab] = crontab
      @options[:cron_schedule] =
        crontab.take(LaunchAgentManager::SCHEDULE_PARTS_COUNT).join(' ')
      @options[:executable_path_with_args] =
        crontab.drop(LaunchAgentManager::SCHEDULE_PARTS_COUNT).join(' ')
    end

    def demand(arg, positional: false)
      return @options[arg] unless @options[arg].nil?

      required_arg = if positional then "<#{arg}>"
      else "--#{arg.to_s.gsub(UNDERSCORE_PATTERN, HYPHEN_STRING)}"
      end
      raise OptionParser::MissingArgument, "Required argument: #{required_arg}"
    end

    def positional!(args)
      POSITIONAL.each do |opt|
        self.method(opt).call(args)
        self.demand(opt, positional: true)
      end
    end

    def options?
      ArgumentsParser::FLAGS.any? { |flag| @options.include?(flag) }
    end

    def usage!
      puts @parser
      exit
    end

    # rubocop: disable Metrics/MethodLength
    def self.parse(args = ARGV, _file_path = ARGF, arguments_parser = ArgumentsParser.new)
      arguments_parser.parser.parse!(args)
      if !arguments_parser.options? && ARGV.length != 1
        message = 'A crontab definition is required'
        raise OptionParser::MissingArgument, message
      elsif !args.empty?
        arguments_parser.positional!(args)
      end
      arguments_parser.options
    rescue OptionParser::AmbiguousOption => e
      abort e.message
    rescue OptionParser::ParseError => e
      puts e.message
      arguments_parser.usage!
    end
    # rubocop: enable Metrics/MethodLength
  end
  # class ArgumentsParser
end
# module Cron
