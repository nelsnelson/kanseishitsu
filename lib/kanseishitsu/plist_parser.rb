# encoding: utf-8
# frozen_string_literal: false

# vi: set ft=ruby :
# -*- mode: ruby -*-

# Copyright Nels Nelson 2024 but freely usable (see license)

require 'rexml/document'

require_relative 'cron'

# Define the PlistParser class
# class PlistParser
#   LOCAL_STRING_XPATH = './string'.freeze
#   PROGRAM_ARGUMENTS_XPATH =
#     '//key[text()="ProgramArguments"]/following-sibling::array[1]'.freeze
#   LABEL_XPATH = '//key[text()="Label"]/following-sibling::*[1]'.freeze
#   CALENDAR_XPATH = '//key[text()="StartCalendarInterval"]/following-sibling::*[1]'.freeze
#   SCHEDULE_XPATH_TEMPLATE = './/key[text()="%<key>s"]/following-sibling::integer'.freeze
#   ON_LOGIN = '@login'.freeze

#   # Parse the Plist file at the given path
#   def parse(plist_path)
#     plist = {}
#     doc = REXML::Document.new(File.read(plist_path))
#     label = parse_plist_label(doc)
#     command = parse_program_arguments(doc, label)
#     cron_schedule = parse_calendar_interval(doc)
#     plist[:label] = label unless label.nil?
#     plist[:command] = command unless command.nil?
#     plist[:schedule] = cron_schedule unless cron_schedule.nil?
#     plist
#   end

#   # Parse the label from the Plist file document
#   def parse_plist_label(doc)
#     label_element = REXML::XPath.first(doc, LABEL_XPATH)
#     return nil if label_element.nil?

#     label = label_element.text
#     return nil if label.empty?

#     label
#   end

#   # Parse the program arguments from the Plist file document
#   def parse_program_arguments(doc, label)
#     program_args_elements = REXML::XPath.match(doc, PROGRAM_ARGUMENTS_XPATH)
#     program_args = program_args_elements.flat_map do |node|
#       REXML::XPath.match(node, LOCAL_STRING_XPATH).map(&:text)
#     end
#     program_args.empty? ? label : program_args.join(' ')
#   end

#   # Parse the calendar interval from the Plist file document
#   def parse_calendar_interval(doc)
#     intervals = doc.xpath(CALENDAR_XPATH).first
#     return ON_LOGIN unless intervals

#     Cron::INTERVALS.map do |interval|
#       intervals.at_xpath(format(SCHEDULE_XPATH_TEMPLATE, key: interval)).text rescue '*'
#     end.join(' ')
#   end
# end

# Define class PlistParser
class PlistParser
  EMPTY_STRING = ''.freeze

  def initialize(file_path)
    @file_path = file_path
  end

  def parse
    xml = File.read(@file_path)
    document = REXML::Document.new(xml)
    plist = parse_element(document.root.elements[1]) # the root <dict> inside <plist>
    LaunchAgentPlist.new(
      plist['Label'],
      plist['ProgramArguments'],
      plist['StartInterval'] || plist['CalendarStartInterval']
    )
  end

  private

  def parse_element(element)
    case element.name.to_sym
    when :dict then parse_dict(element)
    when :array then parse_array(element)
    when :string then element.text || EMPTY_STRING
    end
  end

  def parse_dict(dict_element)
    dict = {}
    dict_element.elements.each_slice(2) do |key, value|
      dict[key.text] = parse_element(value)
    end
    dict
  end

  def parse_array(array_element)
    array_element.elements.map do |element|
      parse_element(element)
    end
  end
end
# class PlistParser
