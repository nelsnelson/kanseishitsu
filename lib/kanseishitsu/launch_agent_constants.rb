# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# Copyright Nels Nelson 2024 but freely usable (see license)

# Define the LaunchAgentConstants module
module LaunchAgentConstants
  DEFAULT_ENCODING = 'UTF-8'.freeze
  DOCTYPE_ELEMENT = '<!%<doctypes>s>'.freeze
  DOCTYPES = [
    'DOCTYPE',
    'plist',
    'PUBLIC',
    '"-//Apple//DTD PLIST 1.0//EN"',
    '"http://www.apple.com/DTDs/PropertyList-1.0.dtd"'
  ].freeze
  LABEL_NAMESPACE = %w[com local].freeze
  INTERVALS = %w[Minute Hour Day Month Weekday].freeze
  SCHEDULE_PARTS_COUNT = INTERVALS.length
  CDATA_PATTERN = %r{[<>&'"\n]+}
  SCHEDULE_XPATH_TEMPLATE = './/key[text()="%<key>s"]/following-sibling::integer'.freeze
  KEYS_REQUIRING_CDATA = ['PATH'].freeze
  LAUNCHCTL_TEMPLATE = 'launchctl bootstrap gui/%<uid>s %<plist>s'.freeze
  SUCCESS_MESSAGE = 'Created and enabled launchd job: %<label>s'.freeze
  LAUNCH_AGENTS_DIR_PATH = File.expand_path(File.join(Dir.home, 'Library', 'LaunchAgents'))
  LABEL_XPATH = '//key[text()="Label"]/following-sibling::*[1]'.freeze
  START_CALENDAR_INTERVAL_XPATH =
    '//key[text()="StartCalendarInterval"]/following-sibling::*[1]'.freeze
  WATCH_PATHS_XPATH = '//key[text()="WatchPaths"]/following-sibling::*[1]'.freeze
  LOCAL_STRING_XPATH = './string'.freeze
  PROGRAM_ARGUMENTS_XPATH =
    '//key[text()="ProgramArguments"]/following-sibling::array[1]'.freeze
  INTERVAL_XPATH_TEMPLATE = './key[text()="%<key>s"]/following-sibling::*[1]'.freeze
  BOOTOUT_TEMPLATE = 'launchctl bootout gui/%<uid>s/%<label>s'.freeze
  REMOVE_TEMPLATE = 'launchctl remove gui/%<uid>s/%<label>s'.freeze
  ON_LOGIN = '@login'.freeze
end
# module LaunchAgentConstants
