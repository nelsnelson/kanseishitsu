#! /usr/bin/env ruby
# encoding: utf-8
# frozen_string_literal: false

# vi: set ft=ruby :
# -*- mode: ruby -*-

# Copyright Nels Nelson 2024 but freely usable (see license)

# Usage: watch.rb <path> [handler_program_path]
#
# Options:
#         --show-all                   Show watchers
#     -l, --list                       List launch agent labels
#     -r, --remove=<label>             Remove a watcher by label
#
# Examples:
#
# watch.rb ~/Desktop ~/.local/usr/bin/desktop.rb
# watch.rb --show-all
# watch.rb --list
# watch.rb --remove=com.local.desktop

require_relative 'launch_agent_manager'
require_relative 'watch/argument_parser'

# Define module Watcher
module Watcher
  # Define class LaunchAgentManager
  class LaunchAgentManager < ::LaunchAgentManager
    # Define the plist contents
    # rubocop: disable Metrics/MethodLength
    def define_plist_contents(label, args, *watch_paths)
      definition = {}
      definition['Label'] = label
      definition['WatchPaths'] = watch_paths
      unless (system_path = ENV.fetch('PATH', nil)).nil? || system_path.empty?
        definition['EnvironmentVariables'] = {}
        definition['EnvironmentVariables']['PATH'] = system_path
      end
      if args.length == 1
        definition['Program'] = args.first
      else
        definition['ProgramArguments'] = args
      end
      definition
    end
    # rubocop: enable Metrics/MethodLength

    # Return a list of all user launch agent labels
    def launch_agent_labels
      labels = []

      Dir.glob(File.join(LAUNCH_AGENTS_DIR_PATH, '*.plist')).each do |file_path|
        doc = File.open(file_path) { |file| Nokogiri::XML(file) }
        watch_paths = doc.xpath(WATCH_PATHS_XPATH)
        next if watch_paths.nil? || watch_paths.empty?

        label_node = doc.xpath(LABEL_XPATH)
        labels << label_node.text unless label_node.empty?
      end

      labels
    end
  end
  # class LaunchAgentManager

  # rubocop: disable Metrics/MethodLength
  def main(args = Watcher::ArgumentsParser.parse)
    log.log_level = args[:log_level]
    manager = Watcher::LaunchAgentManager.new(args)
    if args[:show_all]
      manager.show_all_launch_agents
    elsif args[:list]
      manager.list_launch_agent_labels
    elsif args[:remove]
      manager.remove_launch_agent(args[:remove])
    else
      manager.create_launch_agent(
        args[:executable_path_with_args],
        args[:watch_path].first,
        *args[:watch_path]
      )
    end
  end
  # rubocop: enable Metrics/MethodLength
end
# module Watcher

Object.new.extend(Watcher).main if $PROGRAM_NAME == __FILE__
