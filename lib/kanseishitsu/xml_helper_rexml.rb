# encoding: utf-8
# frozen_string_literal: false

# vi: set ft=ruby :
# -*- mode: ruby -*-

# Copyright Nels Nelson 2024 but freely usable (see license)

require 'rexml/document'

# Define module XMLHelperInstanceMethods
module XMLHelperInstanceMethods
  CDATA_PATTERN = /[<>&'"\n]+/
  KEYS_REQUIRING_CDATA = ['PATH'].freeze

  def create_xml_tag(parent, key, value)
    element = case value
    when Hash then create_dict(value)
    when Array then create_array(value)
    else create_simple_element(key, value)
    end
    parent.add_element(element) unless element.nil?
  end

  private

  def create_dict(value)
    dict = REXML::Element.new('dict')
    value.each do |k, v|
      create_xml_tag(dict, k, v)
    end
    dict
  end

  def create_array(value)
    array = REXML::Element.new('array')
    value.each do |v|
      array.add_element('string').add_text(v.to_s)
    end
    array
  end

  def create_simple_element(key, value)
    # Decide whether to wrap value in CDATA tag specifier
    if value.to_s.match(CDATA_PATTERN) || KEYS_REQUIRING_CDATA.include?(key.to_s)
      string_value = REXML::Element.new('string')
      string_value.add(REXML::CData.new(value.to_s))
      string_value
    else
      el = REXML::Element.new(value.class.to_s.downcase)
      el.add_text(value.to_s)
      el
    end
  end
end
# module XMLHelperInstanceMethods
