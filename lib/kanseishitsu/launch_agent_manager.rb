# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# Copyright Nels Nelson 2024 but freely usable (see license)

require 'nokogiri'

require 'fileutils'

require_relative 'launch_agent_constants'
require_relative 'logging'
require_relative 'which'
require_relative 'xml_helper'

# Define module LaunchAgentManagementInstanceMethods
module LaunchAgentManagementInstanceMethods
  include LaunchAgentConstants
  include Which

  # Extract program path and arguments, verifying executable
  # rubocop: disable Metrics/MethodLength
  def extract_program_path(executable_with_args)
    *args = if executable_with_args.respond_to?(:split)
      executable_with_args.split
    else
      executable_with_args
    end
    exe_path = which(args.shift)
    if exe_path.nil? || !File.exist?(exe_path)
      abort "Cannot find executable in path: #{exe_path}"
    end
    if exe_path.nil? || !File.executable?(exe_path) || File.directory?(exe_path)
      abort "Given file is not executable: #{exe_path}"
    end
    args.unshift(exe_path)
    args
  end
  # rubocop: enable Metrics/MethodLength

  # Create the job name based on the given file path
  def derive_job_label_from_file_path(file_path)
    label = LABEL_NAMESPACE.dup
    label << File.basename(file_path, '.*')
    label.join('.')
  end

  # Define cron settings for plist
  def define_calendar_interval(cron_schedule)
    cron_fields = cron_schedule.split
    calendar_interval = {}
    INTERVALS.each do |key|
      value = cron_fields.shift
      raise "Invalid cron string: #{cron_schedule}" if value.nil? || value.empty?

      calendar_interval[key] = value
    end
    calendar_interval
  end

  # Define the plist contents
  # rubocop: disable Metrics/MethodLength
  def define_plist_contents(label, args, cron_schedule)
    definition = {}
    definition['Label'] = label
    definition['StartCalendarInterval'] = define_calendar_interval(cron_schedule)
    unless (system_path = ENV.fetch('PATH', nil)).nil? || system_path.empty?
      definition['EnvironmentVariables'] = {}
      definition['EnvironmentVariables']['PATH'] = system_path
    end
    if args.length == 1
      definition['Program'] = args.first
    else
      definition['ProgramArguments'] = args
    end
    definition
  end
  # rubocop: enable Metrics/MethodLength

  # Save plist file to LaunchAgents directory; do not overwrite an
  # already existing file with the same name at the generated path
  def save_plist(label, doc)
    FileUtils.mkdir_p(LAUNCH_AGENTS_DIR_PATH)
    plist_path = File.join(LAUNCH_AGENTS_DIR_PATH, "#{label}.plist")
    return plist_path if File.exist?(plist_path)

    log.debug "Contents of plist xml document:\n#{doc.to_xml}"
    File.write(plist_path, doc.to_xml)
    plist_path
  end

  def execute(command)
    log.debug "Executing command: #{command}"
    system(command)
  end

  # Load the launchd job
  def load_launchd_job(plist_path)
    execute(format(LAUNCHCTL_TEMPLATE, uid: Process.uid, plist: plist_path))
  end

  def doctype
    format(DOCTYPE_ELEMENT, doctypes: DOCTYPES.join(' '))
  end

  # Use Nokogiri to create a launchd plist XML document
  def generate_plist_xml(definition_hash)
    doc = Nokogiri::XML(doctype)
    doc.encoding = DEFAULT_ENCODING
    plist = doc.create_element('plist', version: '1.0')
    root = doc.create_element('dict')

    definition_hash.each do |key, value|
      create_xml_tag(doc, root, key, value)
    end

    plist << root
    doc << plist
    doc
  end

  # Return a list of all user launch agent labels
  def launch_agent_labels
    labels = []

    Dir.glob(File.join(LAUNCH_AGENTS_DIR_PATH, '*.plist')).each do |file_path|
      doc = File.open(file_path) { |file| Nokogiri::XML(file) }
      label_node = doc.xpath(LABEL_XPATH)
      labels << label_node.text unless label_node.empty?
    end

    labels
  end

  # Parse the calendar interval
  def parse_schedule(doc)
    watch_paths_node = doc.xpath(WATCH_PATHS_XPATH)
    watch_paths = watch_paths_node.xpath(LOCAL_STRING_XPATH).map(&:text)
    return watch_paths.first unless watch_paths.empty?

    intervals = doc.xpath(START_CALENDAR_INTERVAL_XPATH).first
    return ON_LOGIN unless intervals

    INTERVALS.map do |interval|
      xpath = format(INTERVAL_XPATH_TEMPLATE, key: interval)
      intervals.at_xpath(xpath).text rescue '*'
    end.join(' ')
  end

  # Function to parse plist and extract crontab-like schedule
  # rubocop: disable Metrics/AbcSize
  # rubocop: disable Metrics/MethodLength
  def parse_plist(plist_path)
    plist = {}
    doc = Nokogiri::XML(File.read(plist_path))
    label = doc.xpath(LABEL_XPATH).text
    watch_paths = doc.xpath(WATCH_PATHS_XPATH).xpath(LOCAL_STRING_XPATH).map(&:text)
    program_args = doc.xpath(PROGRAM_ARGUMENTS_XPATH).xpath(LOCAL_STRING_XPATH).map(&:text)
    schedule = parse_schedule(doc)
    plist[:label] = label unless label.nil?
    plist[:command] = program_args.empty? ? label : program_args.join(' ')
    plist[:watch_paths] = watch_paths unless watch_paths.nil?
    plist[:schedule] = schedule unless schedule.nil?
    plist
  end
  # rubocop: enable Metrics/AbcSize
  # rubocop: enable Metrics/MethodLength

  # Show the relevant launch agents
  def show_all_launch_agents
    Dir.glob(File.join(LAUNCH_AGENTS_DIR_PATH, '*.plist')).map do |plist_path|
      job = parse_plist(plist_path)
      puts "#{job[:schedule]} #{job[:command]}"
    end
  end

  # List labels of launch agents
  def list_launch_agent_labels
    launch_agent_labels.each do |label|
      puts label
    end
  end

  # Remove plist Launch Agents by label
  # rubocop: disable Metrics/MethodLength
  def remove_launch_agent(label)
    log.debug "Removing launch agent: #{label}"
    plist_path = File.expand_path(File.join(LAUNCH_AGENTS_DIR_PATH, "#{label}.plist"))
    log.debug "Removing launch agent plist definition file: #{plist_path}"
    if File.exist?(plist_path)
      execute(format(BOOTOUT_TEMPLATE, uid: Process.uid, label: label))
      execute(format(REMOVE_TEMPLATE, uid: Process.uid, label: label))
      FileUtils.rm(plist_path)
      puts "Removed launch agent: #{label}"
    else
      warn "Not found; crontab or launch agent: #{label}"
    end
  end
  # rubocop: enable Metrics/MethodLength

  # Create a file monitor launch agent
  def create_launch_agent(exe, exe_path, schedule)
    args = extract_program_path(exe)
    label = derive_job_label_from_file_path(exe_path || args.first)
    definition = define_plist_contents(label, args, schedule)
    doc = generate_plist_xml(definition)
    plist_path = save_plist(label, doc)
    load_launchd_job(plist_path)
    puts format(SUCCESS_MESSAGE, label: label)
  end
end
# module LaunchAgentManagementInstanceMethods

# Define class LaunchAgentManager
class LaunchAgentManager
  include LaunchAgentManagementInstanceMethods
  include XMLHelperInstanceMethods

  def initialize(options)
    @options = options
  end
end
