# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# Copyright Nels Nelson 2024 but freely usable (see license)

require 'logger'
require 'syslog/logger'

# Define class MultiLogger
class MultiLogger
  def initialize(invoking_class)
    @invoking_class = invoking_class
  end

  %i[info warn debug error fatal].each do |method|
    define_method(method) do |message|
      MultiLogger.loggers.each do |logger|
        logger.add(Logger.const_get(method.upcase), message, @invoking_class || 'Unknown')
      end
    end
  end

  def log_level=(level)
    MultiLogger.loggers.each { |logger| logger.level = level }
  end

  def self.loggers
    @loggers ||= begin
      stdout_logger = Logger.new($stdout)
      stdout_logger.level = Logger::INFO
      stdout_logger.formatter = proc do |severity, datetime, progname, msg|
        "#{datetime} #{severity} [#{progname}] #{msg}\n"
      end
      syslog_logger = Syslog::Logger.new(PROJECT)
      syslog_logger.level = Logger::INFO
      [stdout_logger, syslog_logger]
    end
  end
end

# Re-open class Object
class Object
  def log
    @log ||= MultiLogger.new(self.class.name)
  end
end
