#! /usr/bin/env bash

# Exit if any command returns a non-zero exit status
set -e

# Exit if an unset variable is used
set -u

# Exit if any command in a series of piped commands fails
set -o pipefail

# Define the path to the version file
version_file=$(ls lib/*/version.rb)

# Extract the module path and filename
module_path=$(dirname $version_file)

# Extract the current version number
version=$(ruby -r $module_path/version -I . -e 'puts ::Kanseishitsu::VERSION')

# Parse the version string (assuming semantic versioning)
IFS='.' read -ra ADDR <<< "$version"
major=${ADDR[0]}
minor=${ADDR[1]}
patch=${ADDR[2]}

# Bump the version number
let patch+=1
new_version="$major.$minor.$patch"

# Update the version number in the module
sed -i "s/$version/$new_version/" $version_file

# Stage the file, commit, and push
git checkout -b release-$new_version
git add $version_file
git commit -m "Prepare release version $new_version"
git mr

echo "Version updated to $new_version and pushed to origin."
