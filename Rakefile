# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'rake'
require 'rake/clean'

PROJECT = File.basename(__dir__) unless defined?(PROJECT)
UNDERSCORE_PATTERN = '_'.freeze unless defined?(UNDERSCORE_PATTERN)
HYPHEN_STRING = '-'.freeze unless defined?(HYPHEN_STRING)

load "#{PROJECT.gsub(UNDERSCORE_PATTERN, HYPHEN_STRING)}.gemspec"

CLEAN.add File.join('tmp', '**', '*'), 'tmp'
CLOBBER.add '*.gem', 'pkg'

task default: %i[package]

desc 'Run the rubocop linter'
task :lint do
  system('bundle', 'exec', 'rubocop') or abort
end

desc 'Run the spec tests'
task :test do
  system('bundle', 'exec', 'rspec', '--exclude-pattern', 'spec/verify/**/*_spec.rb') or abort
end
task test: :lint

desc 'Explode the gem'
task :explode do
  system('gem', 'install', '--no-document', '--install-dir=tmp', '*.gem')
end
task explode: :clean

desc 'Package the gem'
task :package do
  system('gem', 'build')
end
task package: %i[clean clobber test]

desc 'Verify the gem'
task :verify do
  system('bundle', 'exec', 'rspec', 'spec/verify') or abort
end
task verify: :explode

desc 'Publish the gem'
task :publish do
  system('jgem', 'push', latest_gem)
end
task publish: :verify

def latest_gem
  `ls -t #{PROJECT.gsub(UNDERSCORE_PATTERN, HYPHEN_STRING)}*.gem`.strip.split("\n").first
end
