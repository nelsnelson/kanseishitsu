FROM ruby:3.3.1-alpine

WORKDIR /usr/src/kanseishitsu

RUN apk --update add --no-cache --virtual run-dependencies \
  bash \
  build-base \
  g++ \
  gcc \
  gcompat \
  git \
  libc-dev \
  make \
  ruby-dev

COPY . .
ENV BUNDLE_APP_CONFIG=/usr/src/kanseishitsu/.bundle
RUN gem update --system \
  && \
  bundle install

COPY spec/verify/launchctl.rb /usr/local/bin/launchctl

CMD ["/bin/bash"]
