# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

RSpec.describe 'test' do
  context 'when required' do
    it 'loads without errors' do
      expect do
        require_relative '../lib/kanseishitsu'
      end.not_to raise_error
    end
  end

  context 'when ran' do
    it 'passes' do
      expect(false).not_to be(true)
    end
  end
end
