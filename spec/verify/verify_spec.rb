# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# Copyright Nels Nelson 2024 but freely usable (see license)

require 'open3'

class Command
  attr_reader :cmd, :stdin, :stdout, :stderr, :pid, :wait_thread

  def initialize(*cmd)
    @cmd = cmd
  end

  def execute_or_raise
    process = execute
    return process if process.errors.empty?

    error_message = process.errors.join(' ')
    raise StandardError, error_message
  end

  # rubocop: disable Metrics/AbcSize
  # rubocop: disable Metrics/MethodLength
  def execute
    Open3.popen3(*cmd) do |_stdin, stdout, stderr, wait_thread|
      @stdin = []
      @stdout = []
      @stderr = []
      @wait_thread = wait_thread
      @pid = wait_thread.pid

      stdout_thread = Thread.new do
        until (line = stdout.gets).nil?
          @stdout << line
          puts line.strip
        end
      end
      stderr_thread = Thread.new do
        until (line = stderr.gets).nil?
          @stderr << line.strip
        end
      end
      stdout_thread.join
      stderr_thread.join

      self
    end
  end
  # rubocop: enable Metrics/AbcSize
  # rubocop: enable Metrics/MethodLength

  def output
    @stdout.join("\n")
  end

  def errors
    @stderr
  end

  def alive?
    @wait_thread.alive?
  end

  def success?
    @wait_thread.value.success?
  end

  def destroy(force: false)
    return unless alive?

    Process.kill('TERM', @pid)
    @wait_thread.join # Give process a chance to exit cleanly
    return unless force && alive?

    Process.kill('KILL', @pid)
  end
end

# rubocop: disable Metrics/BlockLength
RSpec.describe 'verify gem' do
  let(:gem_path) { 'tmp' }
  let(:test_label) { 'com.local.date' }
  let(:expectation_pattern) { Regexp.new(test_label) }

  context 'when invoking the executable' do
    it 'should support crontab creation' do
      env = {}
      env['GEM_PATH'] = gem_path
      env['PATH'] = [ENV.fetch('PATH', nil), File.join('tmp', 'bin')].compact.join(':')

      process = Command.new(env, 'cron.rb', '--list').execute_or_raise

      if expectation_pattern.match?(process.output)
        Command.new(env, 'cron.rb', "--remove=#{test_label}").execute_or_raise
      end

      Command.new(env, 'cron.rb', '"0 0 * * * date"').execute_or_raise

      process = Command.new(env, 'cron.rb', '--list').execute

      results = process.output
      expect(results).to match(expectation_pattern)
    rescue Interrupt => e
      puts "\n#{e.class}"
    rescue StandardError => e
      warn "Unexpected #{e.class}: #{e.message}"
      raise e
    ensure
      Command.new(env, 'cron.rb', "--remove=#{test_label}").execute

      if defined?(process) && !process.nil?
        begin
          puts "Destroying process #{process.pid}"
          process.destroy
          process.destroy(force: true) if process.alive?
        rescue StandardError => e
          warn "Unexpected #{e.class}: #{e.message}"
          raise e
        end
      end
    end
    # it 'should support crontab creation'
  end

  context 'when the gem is required' do
    let(:test_label) { 'com.local.date' }
    let(:expectation) { "#{test_label}\n" }

    it 'loads without errors' do
      expect do
        require 'kanseishitsu'
      end.not_to raise_error
    end

    it 'supports instantiated receivers' do
      require 'kanseishitsu'
      target = Cron::LaunchAgentManager.new({})
      allow(target).to receive(:launch_agent_labels).and_return([test_label])
      expect do
        target.list_launch_agent_labels
      end.to output(expectation).to_stdout
    end
  end
end
# RSpec.describe 'verify gem'
# rubocop: enable Metrics/BlockLength
