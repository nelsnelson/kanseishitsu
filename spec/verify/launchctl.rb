#! /usr/bin/env ruby
# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# Copyright Nels Nelson 2024 but freely usable (see license)

require 'logger'
require 'json'

# Define class LaunchCtlStub
class LaunchCtlStub
  DEFAULT_JOBS_STORE_FILE_PATH = '/tmp/launchctl_jobs.json'.freeze

  def initialize(jobs_file = DEFAULT_JOBS_STORE_FILE_PATH)
    @jobs_file = jobs_file
    @jobs = load_jobs
    @logger = Logger.new($stdout)
    @logger.info "#{self.class} initialized"
  end

  def load_jobs
    return {} unless File.exist?(@jobs_file)

    JSON.parse(File.read(@jobs_file))
  end

  def save_jobs
    File.write(@jobs_file, JSON.generate(@jobs))
  end

  def bootstrap(label, _plist_path)
    @jobs[label] = "Simulated job for #{label}"
    @logger.info "Bootstrapped job: #{label}"
    puts "Bootstrapped job: #{label}"
    save_jobs
  end

  def bootout(label)
    if @jobs.key?(label)
      @jobs.delete(label)
      @logger.info "Booted out job: #{label}"
      puts "Booted out job: #{label}"
      save_jobs
    else
      @logger.warn "Job not found: #{label}"
      puts "Job not found: #{label}"
    end
  end

  def remove(label)
    if @jobs.key?(label)
      @jobs.delete(label)
      @logger.info "Removed job: #{label}"
      puts "Removed job: #{label}"
      save_jobs
    else
      @logger.warn "Job not found: #{label}"
      puts "Job not found: #{label}"
    end
  end

  # rubocop: disable Metrics/MethodLength
  def execute(command, label, plist_path = nil)
    case command.to_sym
    when :bootstrap
      bootstrap(label, plist_path)
    when :bootout
      bootout(label)
    when :remove
      remove(label)
    else
      @logger.error("Unknown command: #{command}")
      puts "Unknown command: #{command}"
    end
  end
  # rubocop: enable Metrics/MethodLength
end

module LaunchCtl
  def main
    if ARGV.size < 2
      puts "Usage: #{$PROGRAM_NAME} <command> <label> [plist_path]"
      exit 1
    end

    command = ARGV[0]
    label = ARGV[1]
    plist_path = ARGV[2]

    stub = LaunchCtlStub.new
    stub.execute(command, label, plist_path)
  end
end

Object.new.extend(LaunchCtl).main if $PROGRAM_NAME == __FILE__
